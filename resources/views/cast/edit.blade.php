@extends('layout.master')

@section('sitetitle','Dashboard')

@section('title')
Edit Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="post">

    @csrf
    @method('PUT')

    <label for="nama">Nama : </label>
    <input type="text" id="nama" name="nama" class="form-control" value="{{$cast->nama}}"><br><br>
    @error('nama')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
    @enderror

    <label for="umur">umur : </label>
    <input type="number" id="umur" name="umur" class="form-control" min=17 max=100 value="{{$cast->umur}}"><br><br>
    @error('umur')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
    @enderror

    <label for="bio">Bio</label><br>
    <textarea name="bio" id="bio" class="form-control" cols="30" rows="10">{{$cast->bio}}</textarea><br><br>
    @error('bio')
    <div class="alert alert-danger">
        {{ $message }}
    </div>
    @enderror

    <input type="submit" class="btn btn-success" value="Edit Cast">

</form>
@endsection
